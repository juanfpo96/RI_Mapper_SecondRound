package uo.ri.persistence;

import uo.ri.model.Suministro;
import uo.ri.model.types.SuministroKey;
import uo.ri.persistence.util.Jpa;

/**
 * Created by Fran on 04/01/2017.
 */
public class SuministroFinder {

	public static Suministro findByID(
			Long idProveedor, Long idRepuesto) {
		return Jpa.getManager().find(Suministro.class,
				new SuministroKey(idProveedor, idRepuesto));
	}
}
