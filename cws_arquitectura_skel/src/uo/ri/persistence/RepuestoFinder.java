package uo.ri.persistence;

import uo.ri.model.Repuesto;
import uo.ri.persistence.util.Jpa;

import java.util.List;

/**
 * Created by Fran on 08/01/2017.
 */
public class RepuestoFinder {
	public static Repuesto findById(
			Long id) {
		return Jpa.getManager().find(Repuesto.class, id);
	}

	public static List<Repuesto> findByDescripcion(
			String descripcion) {
		return Jpa.getManager()
				.createNamedQuery("Repuesto.findByDescripcion", Repuesto.class)
				.setParameter(1, "%"+descripcion+"%").getResultList();
	}

	public static Repuesto findByCodigo(
			String codigo) {
		return Jpa.getManager()
				.createNamedQuery("Repuesto.findByCodigo", Repuesto.class)
				.setParameter(1, codigo).getResultList().stream().findFirst()
				.orElse(null);
	}

	public static List<Repuesto> findExistenciasBajas() {
		List<Repuesto> list = Jpa.getManager()
				.createNamedQuery("Repuesto.findExistenciasBajas",
						Repuesto.class).getResultList();
		return list;
	}
}
