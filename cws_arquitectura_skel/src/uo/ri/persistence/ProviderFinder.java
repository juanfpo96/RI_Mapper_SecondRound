package uo.ri.persistence;

import uo.ri.model.Proveedor;
import uo.ri.persistence.util.Jpa;

import java.util.List;

/**
 * Created by Fran on 04/01/2017.
 */
public class ProviderFinder {
	public static List<Proveedor> findAll() {
		List<Proveedor> list = Jpa.getManager()
				.createNamedQuery("Proveedor.findAll", Proveedor.class)
				.getResultList();
		return list;
	}

	public static Proveedor findByCodigo(
			String codigo) {
		return Jpa.getManager()
				.createNamedQuery("Proveedor.findByCodigo", Proveedor.class)
				.setParameter(1, codigo).getResultList().stream().findFirst()
				.orElse(null);

	}

	public static Proveedor findById(
			Long id) {
		return Jpa.getManager().find(Proveedor.class, id);
	}
}
