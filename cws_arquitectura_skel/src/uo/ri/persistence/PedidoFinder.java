package uo.ri.persistence;

import uo.ri.model.Pedido;
import uo.ri.persistence.util.Jpa;

/**
 * Created by Fran on 05/01/2017.
 */
public class PedidoFinder {

	public static Object findByCodigo(
			String codigo) {
		return Jpa.getManager()
				.createNamedQuery("Pedido.findByCodigo", Pedido.class)
				.setParameter(1, codigo).getResultList().stream().findFirst()
				.orElse(null);
	}
}
