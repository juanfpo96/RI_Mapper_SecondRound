package uo.ri.persistence;

import uo.ri.model.Mecanico;
import uo.ri.persistence.util.Jpa;

import java.util.List;

public class MecanicoFinder {

	public static Mecanico findById(
			Long id) {
		return Jpa.getManager().find(Mecanico.class, id);
	}

	public static List<Mecanico> findAll() {
		List<Mecanico> list = Jpa.getManager()
				.createNamedQuery("Mecanico.findAll", Mecanico.class)
				.getResultList();
		return list;
	}

}
