package uo.ri.persistence;

import uo.ri.model.Averia;
import uo.ri.persistence.util.Jpa;

import java.util.List;

public class AveriaFinder {

	public static List<Averia> findByIds(
			List<Long> idsAveria) {
		return Jpa.getManager()
				.createNamedQuery("Averia.findByIds", Averia.class)
				.setParameter(1, idsAveria).getResultList();
	}

	public static List<Averia> findNoFacturadasByDni(
			String dni) {

		return Jpa.getManager()
				.createNamedQuery("Averia.findNoFacturadasByDNI", Averia.class)
				.setParameter(1, dni).getResultList();
	}

}
