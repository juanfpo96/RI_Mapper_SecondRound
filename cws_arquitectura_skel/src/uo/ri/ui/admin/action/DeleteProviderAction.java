package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;

/**
 * Created by Fran on 06/12/2016.
 */
public class DeleteProviderAction implements Action {
	@Override public void execute()
			throws Exception {
		Long idProveedor = Console.readLong("Id de proveedor");

		AdminService aS = ServicesFactory.getAdminService();
		aS.deleteProvider(idProveedor);

		Console.println("Se ha eliminado el proveedor");
	}
}
