package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;

/**
 * Created by juanf on 21/04/2017.
 */
public class UpdateRepuestoAction implements Action {
	@Override public void execute()
			throws Exception {
		Long id = Console.readLong("Id del repuesto");
		String descripcion = Console.readString("Descripcion");
		double precio = Console.readDouble("Precio");
		int existencias = Console.readInt("Existencias");
		int maxExistencias = Console.readInt("Max. Existencias");
		int minExistencias = Console.readInt("Min. Existencias");

		AdminService aS = ServicesFactory.getAdminService();
		Repuesto p = aS.findRepuestoById(id);
		assertNotNull(p);
		p.setDescripcion(descripcion);
		p.setPrecio(precio);
		p.setExistencias(existencias);
		p.setMaxExistencias(maxExistencias);
		p.setMinExistencias(minExistencias);
		aS.updateRepuesto(p);

		// Mostrar resultado
		Console.println("Proveedor actualizado");
	}

	private void assertNotNull(
			Repuesto p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El proveedor no existe");
	}
}
