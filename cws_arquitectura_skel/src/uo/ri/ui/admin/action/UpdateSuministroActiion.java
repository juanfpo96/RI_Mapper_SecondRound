package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Suministro;

/**
 * Created by Fran on 05/12/2016.
 */
public class UpdateSuministroActiion implements Action {

	@Override public void execute()
			throws Exception {
		Long idRepuesto = Console.readLong("Id de repuesto");
		Long idProveedor = Console.readLong("Id de proveedor");
		Double precio = Console.readDouble("Precio");

		AdminService aS = ServicesFactory.getAdminService();
		Suministro s = aS.findSuministroById(idRepuesto, idProveedor);
		s.setPrecio(precio);
		aS.updateSuministro(s);

		// Mostrar resultado
		Console.println("Suministro actualizado");
	}
}
