package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;
import uo.ri.ui.util.Printer;

import java.util.List;

/**
 * Created by juanf on 23/04/2017.
 */
public class ListRepuestoExistenciasBajasAction implements Action {
	@Override public void execute()
			throws Exception {

		Console.println("\nListado de repuestos\n");

		AdminService aS = ServicesFactory.getAdminService();

		List<Repuesto> r = aS.findRepuestoExistenciasBajas();

		assertNotEmpty(r);

		r.forEach(Printer::printRepuesto);

	}

	private void assertNotEmpty(
			List<Repuesto> r)
			throws BusinessException {
		if (r.size() > 0)
			return;
		throw new BusinessException("No existen repuestos con "
				+ "existencias bajas");
	}
}
