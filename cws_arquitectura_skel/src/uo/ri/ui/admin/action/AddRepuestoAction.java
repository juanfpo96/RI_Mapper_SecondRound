package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Repuesto;

/**
 * Created by juanf on 21/04/2017.
 */
public class AddRepuestoAction implements Action {
	@Override public void execute()
			throws Exception {

		String codigo = Console.readString("Codigo");
		String descripcion = Console.readString("Descripcion");
		double precio = Console.readDouble("Precio");
		int existencias = Console.readInt("Existencias");
		int maxExistencias = Console.readInt("Max. Existencias");
		int minExistencias = Console.readInt("Min. Existencias");

		AdminService as = ServicesFactory.getAdminService();
		Repuesto r = new Repuesto(codigo, descripcion, precio);
		r.setMaxExistencias(maxExistencias);
		r.setMinExistencias(minExistencias);
		r.setExistencias(existencias);
		as.newRepuesto(r);

		// Mostrar resultado
		Console.println("Nuevo mecánico añadido");
	}
}
