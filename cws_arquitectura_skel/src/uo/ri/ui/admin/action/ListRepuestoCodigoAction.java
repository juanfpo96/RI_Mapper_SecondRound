package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;
import uo.ri.ui.util.Printer;

/**
 * Created by juanf on 23/04/2017.
 */
public class ListRepuestoCodigoAction implements Action {
	@Override public void execute()
			throws Exception {
		String codigo = Console.readString("Codigo del repuesto");
		Console.println("\nListado de repuestos\n");

		AdminService aS = ServicesFactory.getAdminService();
		Repuesto r = aS.findRepuestoByCodigo(codigo);

		assertNotNull(r);

		Printer.printRepuesto(r);
	}

	private void assertNotNull(
			Repuesto p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El repuesto no existe");
	}
}
