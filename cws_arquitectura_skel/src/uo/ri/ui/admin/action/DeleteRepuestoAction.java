package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;

/**
 * Created by juanf on 21/04/2017.
 */
public class DeleteRepuestoAction implements Action {
	@Override public void execute()
			throws Exception {
		Long idRepuesto = Console.readLong("Id de repuesto");

		AdminService aS = ServicesFactory.getAdminService();
		aS.deleteRepuesto(idRepuesto);

		Console.println("Se ha eliminado el proveedor");
	}
}
