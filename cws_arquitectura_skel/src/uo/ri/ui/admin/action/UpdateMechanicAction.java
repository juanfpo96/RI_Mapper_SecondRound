package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Mecanico;
import uo.ri.model.exception.BusinessException;

public class UpdateMechanicAction implements Action {

	@Override public void execute()
			throws BusinessException {

		// Pedir datos
		Long id = Console.readLong("Id del mecánico");
		String nombre = Console.readString("Nombre");
		String apellidos = Console.readString("Apellidos");

		// Procesar
		AdminService as = ServicesFactory.getAdminService();

		Mecanico m = as.findMechanicById(id);
		assertNotNull(m);
		m.setNombre(nombre);
		m.setApellidos(apellidos);

		as.updateMechanic(m);

		// Mostrar resultado
		Console.println("Mecánico actualizado");
	}

	private void assertNotNull(
			Mecanico p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El proveedor no existe");
	}
}
