package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Proveedor;

/**
 * Created by Fran on 06/12/2016.
 */
public class AddProviderAction implements Action {
	@Override public void execute()
			throws Exception {

		// Pedir datos
		String nombre = Console.readString("Nombre");
		String codigo = Console.readString("Codigo");

		AdminService aS = ServicesFactory.getAdminService();
		aS.newProvider(new Proveedor(codigo, nombre));

		// Mostrar resultado
		Console.println("Nuevo proveedor añadido");

	}
}
