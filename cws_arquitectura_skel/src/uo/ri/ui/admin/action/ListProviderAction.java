package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Proveedor;
import uo.ri.ui.util.Printer;

import java.util.List;

/**
 * Created by Fran on 06/12/2016.
 */
public class ListProviderAction implements Action {
	@Override public void execute()
			throws Exception {
		Console.println("\nListado de proveedores\n");

		AdminService aS = ServicesFactory.getAdminService();
		List<Proveedor> proveedores = aS.findAllProviders();

		for (Proveedor p : proveedores) {
			Printer.printProvider(p);
		}
	}
}
