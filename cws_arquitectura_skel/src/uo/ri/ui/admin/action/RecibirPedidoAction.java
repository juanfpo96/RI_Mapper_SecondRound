package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Pedido;
import uo.ri.ui.util.Printer;

/**
 * Created by Fran on 07/12/2016.
 */
public class RecibirPedidoAction implements Action {
	@Override public void execute()
			throws Exception {
		String codigo = Console.readString("Codigo del pedido");

		AdminService aS = ServicesFactory.getAdminService();
		Pedido pedido = aS.findPedidoByCodigo(codigo);

		Printer.printPedidoDetallado(pedido);

		if (Console.readString("¿Es correcto el pedido? (s/n) ")
				.equalsIgnoreCase("s")) {
			aS.UpdatePedido(pedido);
		}
	}
}
