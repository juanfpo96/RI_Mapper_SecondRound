package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Proveedor;
import uo.ri.model.Repuesto;
import uo.ri.model.Suministro;
import uo.ri.model.exception.BusinessException;

/**
 * Created by Fran on 05/12/2016.
 */
public class AddSuministroAction implements Action {

	@Override public void execute()
			throws BusinessException {
		Long idRepuesto = Console.readLong("Id de repuesto");
		Long idProveedor = Console.readLong("Id de proveedor");
		Double precio = Console.readDouble("Precio");

		AdminService aS = ServicesFactory.getAdminService();
		Proveedor p = aS.findProviderById(idProveedor);
		assertNotNull(p);
		Repuesto r = aS.findRepuestoById(idRepuesto);
		assertNotNull(r);
		aS.newSuministro(new Suministro(r, p, precio));

		Console.println("Nuevo suministro añadido");
	}

	private void assertNotNull(
			Proveedor p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El proveedor no existe");
	}

	private void assertNotNull(
			Repuesto p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El repuesto no existe");
	}
}
