package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Proveedor;
import uo.ri.model.Suministro;
import uo.ri.model.exception.BusinessException;
import uo.ri.ui.util.Printer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fran on 05/12/2016.
 */
public class ListSuministroProveedorAction implements Action {
	@Override public void execute()
			throws Exception {

		Long idProveedor = Console.readLong("Id de proveedor");

		Console.println("\nListado de suministros\n");

		AdminService aS = ServicesFactory.getAdminService();
		Proveedor p = aS.findProviderById(idProveedor);
		assertNotNull(p);
		List<Suministro> suministros = new ArrayList<>();
		suministros.addAll(p.getSuministros());

		for (Suministro suministro : suministros) {
			Printer.printSuministro(suministro);

		}
	}

	private void assertNotNull(
			Proveedor p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El proveedor no existe");
	}
}
