package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.exception.BusinessException;

/**
 * Created by Fran on 05/12/2016.
 */
public class DeleteSuministroAction implements Action {

	@Override public void execute()
			throws BusinessException {
		Long idRepuesto = Console.readLong("Id de repuesto");
		Long idProveedor = Console.readLong("Id de proveedor");
		AdminService aS = ServicesFactory.getAdminService();
		aS.deleteSuministro(idRepuesto, idProveedor);

		Console.println("Se ha eliminado el suministro");
	}

}