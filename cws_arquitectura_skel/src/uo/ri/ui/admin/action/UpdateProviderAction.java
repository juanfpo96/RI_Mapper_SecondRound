package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Proveedor;
import uo.ri.model.exception.BusinessException;

/**
 * Created by Fran on 06/12/2016.
 */
public class UpdateProviderAction implements Action {
	@Override public void execute()
			throws Exception {
		Long id = Console.readLong("Id del proveedor");
		String nombre = Console.readString("Nombre");

		AdminService aS = ServicesFactory.getAdminService();
		Proveedor p = aS.findProviderById(id);
		assertNotNull(p);
		p.setNombre(nombre);
		aS.updateProvider(p);

		// Mostrar resultado
		Console.println("Proveedor actualizado");
	}

	private void assertNotNull(
			Proveedor p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El proveedor no existe");
	}
}
