package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Pedido;
import uo.ri.model.Proveedor;
import uo.ri.ui.util.Printer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Fran on 07/12/2016.
 */
public class ListPedidosProveedorAction implements Action {

	@Override public void execute()
			throws Exception {
		String codigo = Console.readString("Codigo de proveedor");

		AdminService aS = ServicesFactory.getAdminService();
		Proveedor p = aS.findProviderByCodigo(codigo);
		List<Pedido> pedidos = new ArrayList<>();
		pedidos.addAll(p.getPedidos());
		pedidos.sort(Comparator.comparing(Pedido::getEstado)
				.thenComparing(Pedido::getFechaCreacion));

		Console.println("\nListado de Pedidos\n");

		Printer.printProvider(p);

		for (Pedido pedido : pedidos) {
			Printer.printPedido(pedido);
		}
	}
}
