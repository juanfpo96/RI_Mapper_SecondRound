package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Pedido;
import uo.ri.ui.util.Printer;

import java.util.List;

/**
 * Created by juanf on 23/04/2017.
 */
public class GenerarPedidoAction implements Action {
	@Override public void execute()
			throws Exception {

		AdminService aS = ServicesFactory.getAdminService();

		List<Pedido> pedidos = aS.generarPedido();

		Console.println("\nLista de pedidos realizados\n");

		pedidos.forEach(Printer::printPedido);

		Console.println("\nPedido generado\n");
	}
}
