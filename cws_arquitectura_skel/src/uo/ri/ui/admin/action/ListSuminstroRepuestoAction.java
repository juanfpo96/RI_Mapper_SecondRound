package uo.ri.ui.admin.action;

import alb.util.console.Console;
import alb.util.menu.Action;
import uo.ri.business.AdminService;
import uo.ri.conf.ServicesFactory;
import uo.ri.model.Repuesto;
import uo.ri.model.Suministro;
import uo.ri.model.exception.BusinessException;
import uo.ri.ui.util.Printer;

import java.util.Set;

/**
 * Created by Fran on 05/12/2016.
 */
public class ListSuminstroRepuestoAction implements Action {
	@Override public void execute()
			throws Exception {
		Long idRepuesto = Console.readLong("Id de repuesto");

		Console.println("\nListado de suministros\n");

		AdminService aS = ServicesFactory.getAdminService();
		Repuesto r = aS.findRepuestoById(idRepuesto);
		assertNotNull(r);
		Set<Suministro> suministros = r.getSuministros();

		for (Suministro suministro : suministros) {
			Printer.printSuministro(suministro);

		}
	}

	private void assertNotNull(
			Repuesto p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El repuesto no existe");
	}
}
