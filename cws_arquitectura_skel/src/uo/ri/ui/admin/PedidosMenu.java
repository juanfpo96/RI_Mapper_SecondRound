package uo.ri.ui.admin;

import alb.util.menu.BaseMenu;
import uo.ri.ui.admin.action.GenerarPedidoAction;
import uo.ri.ui.admin.action.ListPedidosProveedorAction;
import uo.ri.ui.admin.action.RecibirPedidoAction;

/**
 * Created by Fran on 05/12/2016.
 */
public class PedidosMenu extends BaseMenu {

	public PedidosMenu() {
		menuOptions =
				new Object[][] { { "Administrador > Gesti�n de pedidos",
						null },

						{ "Generar pedido", GenerarPedidoAction.class },

						{ "Recepci�n de pedido", RecibirPedidoAction.class },
						{ "Listar pedidos de un proveedor",
								ListPedidosProveedorAction.class }, };
	}
}
