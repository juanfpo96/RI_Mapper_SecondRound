package uo.ri.ui.admin;

import alb.util.menu.BaseMenu;
import uo.ri.ui.admin.action.*;

public class RepuestosMenu extends BaseMenu {

	public RepuestosMenu() {
		menuOptions = new Object[][] {
				{ "Administrador > Gesti�n de repuestos", null },

				{ "A�adir repuesto", AddRepuestoAction.class },
				{ "Modificar datos de repuesto", UpdateRepuestoAction.class },
				{ "Eliminar repuesto", DeleteRepuestoAction.class },
				{ "Localizar repuesto por codigo",
						ListRepuestoCodigoAction.class },
				{ "Localizar repuesto por descripcion",
						ListRepuestoDescripcionAction.class },
				{ "Localizar repuesto por existencias bajas",
						ListRepuestoExistenciasBajasAction.class }, };
	}

}
