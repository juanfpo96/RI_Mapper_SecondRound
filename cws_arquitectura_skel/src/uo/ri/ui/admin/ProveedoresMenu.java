package uo.ri.ui.admin;

import alb.util.menu.BaseMenu;
import uo.ri.ui.admin.action.AddProviderAction;
import uo.ri.ui.admin.action.DeleteProviderAction;
import uo.ri.ui.admin.action.ListProviderAction;
import uo.ri.ui.admin.action.UpdateProviderAction;

/**
 * Created by Fran on 05/12/2016.
 */
public class ProveedoresMenu extends BaseMenu {

	public ProveedoresMenu() {
		menuOptions = new Object[][] {
				{ "Administrador > Gesti�n de proveedores", null },

				{ "A�adir proveedor", AddProviderAction.class },
				{ "Modificar datos de proveedor", UpdateProviderAction.class },
				{ "Eliminar proveedor", DeleteProviderAction.class },
				{ "Listar proveedores", ListProviderAction.class }, };
	}
}
