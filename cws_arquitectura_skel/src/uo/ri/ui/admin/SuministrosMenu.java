package uo.ri.ui.admin;

import alb.util.menu.BaseMenu;
import uo.ri.ui.admin.action.*;

/**
 * Created by Fran on 05/12/2016.
 */
public class SuministrosMenu extends BaseMenu {
	public SuministrosMenu() {
		menuOptions = new Object[][] {
				{ "Administrador > Gesti�n de suministros", null },

				{ "A�adir suministro", AddSuministroAction.class },
				{ "Modificar datos de suministro",
						UpdateSuministroActiion.class },
				{ "Eliminar suministro", DeleteSuministroAction.class },
				{ "Listar suministro por proveedor",
						ListSuministroProveedorAction.class },
				{ "Listar suministro por repuesto",
						ListSuminstroRepuestoAction.class }, };
	}
}
