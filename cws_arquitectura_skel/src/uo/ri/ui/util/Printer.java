package uo.ri.ui.util;

import alb.util.console.Console;
import uo.ri.model.*;

import java.util.List;

public class Printer {

	public static void printInvoice(
			Factura invoice) {

		double importeConIVa = invoice.getImporte();
		double iva = (Double) invoice.getIva();
		double importeSinIva = importeConIVa / (1 + iva / 100);

		Console.printf("Factura nº: %d\n", invoice.getNumero());
		Console.printf("\tFecha: %1$td/%1$tm/%1$tY\n", invoice.getFecha());
		Console.printf("\tTotal: %.2f €\n", importeSinIva);
		Console.printf("\tIva: %.1f %% \n", invoice.getIva());
		Console.printf("\tTotal con IVA: %.2f €\n", invoice.getImporte());
		Console.printf("\tEstado: %s\n", invoice.getStatus());
	}

	public static void printPaymentMeans(
			List<MedioPago> medios) {
		Console.println();
		Console.println("Medios de pago disponibles");

		Console.printf("\t%s \t%-8.8s \t%s \n", "ID", "Tipo", "Acumulado");
		for (MedioPago medio : medios) {
			// Console.println( medio.toFormatedString() );
			Console.println(medio.toString());
		}
	}

	public static void printRepairing(
			Averia rep) {

		Console.printf("\t%d \t%-40.40s \t%td/%<tm/%<tY \t%-12.12s \t%.2f\n",
				rep.getId(), rep.getDescripcion(), rep.getFecha(),
				rep.getStatus(), rep.getImporte());
	}

	public static void printMechanic(
			Mecanico m) {

		Console.printf("\t%d %-10.10s %-25.25s %-25.25s\n", m.getId(),
				m.getDni(), m.getNombre(), m.getApellidos());
	}

	public static void printProvider(
			Proveedor p) {

		Console.printf("\t%d %s %s\n", p.getId(), p.getCodigo(), p.getNombre
				());
	}

	public static void printPedido(
			Pedido p) {

		Console.printf("\t\t%s %s %s %s\n", p.getCodigo(),
				p.getFechaCreacion().toString(),
				p.getFechaRecepcion().toString(), p.getEstado());
	}

	public static void printSuministro(
			Suministro suministro) {

		Console.printf("\t%s %s %d\n", suministro.getProveedor().toString(),
				suministro.getRepuesto().toString(), suministro.getPrecio());
	}

	public static void printPedidoDetallado(
			Pedido p) {
		printPedido(p);

		for (DetallePedido dP : p.getDetallePedidos()) {
			Console.printf("\t%s %s %d %.2f\n", dP.getRepuesto().getCodigo(),
					dP.getRepuesto().getDescripcion(), dP.getUnidades(),
					dP.getPrecio());
		}
	}

	public static void printRepuesto(
			Repuesto r) {
		Console.printf("\t\t%s %s %.2f %d %d %d\n", r.getCodigo(),
				r.getDescripcion(), r.getPrecio(), r.getExistencias(),
				r.getMinExistencias(), r.getMaxExistencias());
	}
}
