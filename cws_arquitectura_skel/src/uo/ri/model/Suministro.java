package uo.ri.model;

import uo.ri.model.types.SuministroKey;

import javax.persistence.*;

/**
 * Created by Fran on 03/01/2017.
 */
@Entity
@Table(name = "TSuministros")
@IdClass(SuministroKey.class)
public class Suministro {

	@Id
	@ManyToOne
	@JoinColumn(name = "repuesto_id")
	private Repuesto repuesto;

	@Id
	@ManyToOne
	@JoinColumn(name = "proveedor_id")
	private Proveedor proveedor;

	private Double precio;

	Suministro() {
	}

	public Suministro(Repuesto r, Proveedor p) {
		super();
		Association.Suministrar.link(this, p, r);
	}

	public Suministro(Repuesto r, Proveedor p, Double precio) {
		this(r, p);
		this.precio = precio;
	}

	public
			Double getPrecio() {
		return precio;
	}

	public
			void setPrecio(
					Double precio) {
		this.precio = precio;
	}

	public
			Repuesto getRepuesto() {
		return repuesto;
	}

	public
			Proveedor getProveedor() {
		return proveedor;
	}

	@Override
	public
			boolean equals(
					Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Suministro that = (Suministro) o;

		if (!repuesto.equals(that.repuesto))
			return false;
		return proveedor.equals(that.proveedor);
	}

	@Override
	public
			int hashCode() {
		int result = repuesto.hashCode();
		result = 31 * result + proveedor.hashCode();
		return result;
	}

	@Override
	public
			String toString() {
		return "Suministro{" + "repuesto=" + repuesto + ", proveedor="
				+ proveedor + ", precio=" + precio + '}';
	}

	void
			_setRepuesto(
					Repuesto repuesto) {
		this.repuesto = repuesto;
	}

	void
			_setProveedor(
					Proveedor proveedor) {
		this.proveedor = proveedor;
	}

}
