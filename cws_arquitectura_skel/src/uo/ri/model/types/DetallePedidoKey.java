package uo.ri.model.types;

import java.io.Serializable;

/**
 * Created by Fran on 04/01/2017.
 */
public class DetallePedidoKey implements Serializable {

	private static final long serialVersionUID = 1L;

	Long repuesto;
	Long pedido;

	@Override public boolean equals(
			Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DetallePedidoKey that = (DetallePedidoKey) o;

		if (!repuesto.equals(that.repuesto))
			return false;
		return pedido.equals(that.pedido);
	}

	@Override public int hashCode() {
		int result = repuesto.hashCode();
		result = 31 * result + pedido.hashCode();
		return result;
	}
}
