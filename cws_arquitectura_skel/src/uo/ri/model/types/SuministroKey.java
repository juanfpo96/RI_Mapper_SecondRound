package uo.ri.model.types;

import java.io.Serializable;

/**
 * Created by Fran on 04/01/2017.
 */
public class SuministroKey implements Serializable {

	private static final long serialVersionUID = 1L;

	Long repuesto;
	Long proveedor;

	public SuministroKey(Long idProveedor, Long idRepuesto) {
		this.repuesto = idRepuesto;
		this.proveedor = idProveedor;
	}

	@Override public boolean equals(
			Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SuministroKey that = (SuministroKey) o;

		if (!repuesto.equals(that.repuesto))
			return false;
		return proveedor.equals(that.proveedor);
	}

	@Override public int hashCode() {
		int result = repuesto.hashCode();
		result = 31 * result + proveedor.hashCode();
		return result;
	}
}
