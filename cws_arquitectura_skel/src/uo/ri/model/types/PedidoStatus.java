package uo.ri.model.types;

/**
 * Created by Fran on 03/01/2017.
 */
public enum PedidoStatus {
	REALIZADO, SERVIDO
}
