package uo.ri.model;

import alb.util.date.DateUtil;
import alb.util.math.Round;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.types.AveriaStatus;
import uo.ri.model.types.FacturaStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "TFACTURAS")
public class Factura {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long numero;
	@Temporal(TemporalType.DATE)
	private Date fecha;
	private double importe;
	private double iva;
	@Enumerated(EnumType.STRING)
	private FacturaStatus status = FacturaStatus.SIN_ABONAR;

	@OneToMany(mappedBy = "factura")
	private Set<Averia> averias = new HashSet<>();

	@OneToMany(mappedBy = "factura")
	private Set<Cargo> cargos = new HashSet<>();

	Factura() {
	}

	public Factura(Long numero) {
		super();
		this.numero = numero;
	}

	public Factura(long numero, Date fecha) {
		this(numero);
	}

	public Factura(long numero, List<Averia> averias) throws BusinessException {
		this(numero);
		addAverias(averias);
	}

	public Factura(long numero, Date fecha, List<Averia> averias)
			throws BusinessException {
		this(numero);
		this.fecha = fecha;
		addAverias(averias);
	}

	private
			void addAverias(
					List<Averia> averias)
					throws BusinessException {
		for (Averia averia : averias)
			addAveria(averia);
	}

	public
			Date getFecha() {
		return fecha;
	}

	public
			void setFecha(
					Date fecha) {
		this.fecha = fecha;
	}

	public
			double getImporte() {
		return importe;
	}

	public
			void setImporte(
					double importe) {
		this.importe = importe;
	}

	public
			double getIva() {
		return iva;
	}

	public
			void setIva(
					double iva) {
		this.iva = iva;
	}

	public
			FacturaStatus getStatus() {
		return status;
	}

	public
			void setStatus(
					FacturaStatus status) {
		this.status = status;
	}

	public
			Long getNumero() {
		return numero;
	}

	public
			Set<Averia> getAverias() {
		return new HashSet<>(averias);
	}

	Set<Averia>
			_getAverias() {
		return averias;
	}

	public
			Set<Cargo> getCargos() {
		return new HashSet<>(cargos);
	}

	Set<Cargo>
			_getCargos() {
		return cargos;
	}

	@Override
	public
			int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public
			boolean equals(
					Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Factura other = (Factura) obj;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}

	@Override
	public
			String toString() {
		return "Factura [numero=" + numero + ", fecha=" + fecha + ", importe="
				+ importe + ", iva=" + iva + ", status=" + status + "]";
	}

	/**
	 * A�ade la averia a la factura
	 *
	 * @param averia
	 * @throws BusinessException
	 */
	public
			void addAveria(
					Averia averia)
					throws BusinessException {
		// Verificar que la factura esta en estado SIN_ABONAR
		// Verificar que La averia esta TERMINADA
		// linkar factura y averia
		// marcar la averia como FACTURADA ( averia.markAsInvoiced() )
		// calcular el importe

		if (averia.getStatus() != AveriaStatus.TERMINADA)
			throw new BusinessException("averia no terminada");

		if (getStatus() == FacturaStatus.SIN_ABONAR
				&& averia.getStatus() == AveriaStatus.TERMINADA) {
			Association.Facturar.link(this, averia);
			averia.markAsInvoiced();
			calcularImporte();
		}
	}

	/**
	 * Calcula el importe de la averia y su IVA, teniendo en cuenta la fecha de
	 * factura
	 */
	void
			calcularImporte() {
		// iva = ...
		// importe = ...

		if (fecha == null) {
			setFecha(new Date());
		}

		if (fecha.after(DateUtil.fromString("01/07/2012"))) {
			setIva(0.21);
		} else {
			setIva(0.18);
		}

		double total = 0;
		for (Averia averia : averias) {
			total += averia.getImporte();
		}
		total += total * iva;
		setImporte(Round.twoCents(total));
	}

	/**
	 * Elimina una averia de la factura, solo si esta SIN_ABONAR y recalcula el
	 * importe
	 *
	 * @param averia
	 */
	public
			void removeAveria(
					Averia averia) {
		// verificar que la factura esta sin abonar
		// desenlazar factura y averia
		// la averia vuelve al estado FINALIZADA ( averia.markBackToFinished
		// () )
		// volver a calcular el importe

		if (getStatus() == FacturaStatus.SIN_ABONAR) {
			Association.Facturar.unlink(this, averia);
			averia.markBackToFinished();
			calcularImporte();
		}
	}

}
