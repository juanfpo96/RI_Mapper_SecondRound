package uo.ri.model.exception;

public interface Command {

	Object execute()
			throws BusinessException;
}
