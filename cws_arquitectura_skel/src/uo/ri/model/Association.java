package uo.ri.model;

public class Association {

	public static class Poseer {

		public static
				void link(
						Cliente cliente, Vehiculo vehiculo) {
			vehiculo._setCliente(cliente);
			cliente._getVehiculos().add(vehiculo);

		}

		public static
				void unlink(
						Cliente cliente, Vehiculo vehiculo) {
			cliente._getVehiculos().remove(vehiculo);
			vehiculo._setCliente(null);
		}
	}

	public static class Clasificar {

		public static
				void link(
						TipoVehiculo tipoVehiculo, Vehiculo vehiculo) {
			vehiculo._setTipoVehiculo(tipoVehiculo);
			tipoVehiculo._getVehiculos().add(vehiculo);
		}

		public static
				void unlink(
						TipoVehiculo tipoVehiculo, Vehiculo vehiculo) {
			tipoVehiculo._getVehiculos().remove(vehiculo);
			vehiculo._setTipoVehiculo(null);
		}
	}

	public static class Pagar {

		public static
				void link(
						MedioPago medio, Cliente cliente) {
			medio._setCliente(cliente);
			cliente._getMediosPago().add(medio);

		}

		public static
				void unlink(
						Cliente cliente, MedioPago medio) {
			cliente._getMediosPago().remove(medio);
			medio._setCliente(null);

		}
	}

	public static class Averiar {

		public static
				void link(
						Vehiculo vehiculo, Averia averia) {
			averia._setVehiculo(vehiculo);
			vehiculo._getAverias().add(averia);
		}

		public static
				void unlink(
						Vehiculo vehiculo, Averia averia) {
			vehiculo._getAverias().remove(averia);
			averia._setVehiculo(null);

		}
	}

	public static class Facturar {

		public static
				void link(
						Factura factura, Averia averia) {
			averia._setFactura(factura);
			factura._getAverias().add(averia);
		}

		public static
				void unlink(
						Factura factura, Averia averia) {
			factura._getAverias().remove(averia);
			averia._setFactura(null);
		}
	}

	public static class Cargar {

		public static
				void link(
						MedioPago medioPago, Cargo cargo, Factura factura) {
			cargo._setMedioPago(medioPago);
			cargo._setFactura(factura);

			medioPago._getCargos().add(cargo);
			factura._getCargos().add(cargo);
		}

		public static
				void unlink(
						Cargo cargo) {
			cargo.getMedioPago()._getCargos().remove(cargo);
			cargo.getFactura()._getCargos().remove(cargo);

			cargo._setMedioPago(null);
			cargo._setFactura(null);
		}
	}

	public static class Asignar {

		public static
				void link(
						Mecanico mecanico, Averia averia) {
			averia._setMecanico(mecanico);
			mecanico._getAsignadas().add(averia);

		}

		public static
				void unlink(
						Mecanico mecanico, Averia averia) {
			mecanico._getAsignadas().remove(averia);
			averia._setMecanico(null);
		}
	}

	public static class Intervenir {

		public static
				void link(
						Averia averia, Intervencion intervencion,
						Mecanico mecanico) {
			intervencion._setAveria(averia);
			intervencion._setMecanico(mecanico);

			averia._getIntervenciones().add(intervencion);
			mecanico._getIntervenciones().add(intervencion);

		}

		public static
				void unlink(
						Intervencion intervencion) {
			intervencion.getAveria()._getIntervenciones().remove(intervencion);
			intervencion.getMecanico()._getIntervenciones()
					.remove(intervencion);

			intervencion._setAveria(null);
			intervencion._setMecanico(null);
		}
	}

	public static class Sustituir {

		public static
				void link(
						Repuesto repuesto, Sustitucion sustitucion,
						Intervencion intervencion) {
			sustitucion._setRepuesto(repuesto);
			sustitucion._setIntervencion(intervencion);

			repuesto._getSustituciones().add(sustitucion);
			intervencion._getSustituciones().add(sustitucion);
		}

		public static
				void unlink(
						Sustitucion sustitucion) {
			sustitucion.getRepuesto()._getSustituciones().remove(sustitucion);
			sustitucion.getIntervencion()._getSustituciones()
					.remove(sustitucion);

			sustitucion._setRepuesto(null);
			sustitucion._setIntervencion(null);
		}

	}

	public static class Suministrar {

		public static
				void link(
						Suministro suministro, Proveedor proveedor, Repuesto repuesto) {
			suministro._setRepuesto(repuesto);
			suministro._setProveedor(proveedor);

			repuesto._getSuministros().add(suministro);
			proveedor._getSuministros().add(suministro);
		}

		public static
				void unlink(
						Suministro suministro) {
			suministro.getProveedor()._getSuministros().remove(suministro);
			suministro.getRepuesto()._getSuministros().remove(suministro);

			suministro._setRepuesto(null);
			suministro._setProveedor(null);
		}
	}

	public static class Proveer {

		public static
				void link(
						Proveedor proveedor, Pedido pedido) {
			pedido._setProveedor(proveedor);
			proveedor._getPedidos().add(pedido);
		}

		public static
				void unlink(
						Proveedor proveedor, Pedido pedido) {
			proveedor._getPedidos().remove(pedido);
			pedido._setProveedor(null);
		}
	}

	public static class Detallar {

		public static
				void link(
						DetallePedido detalle, Pedido pedido, Repuesto repuesto) {
			detalle._setRepuesto(repuesto);
			detalle._setPedido(pedido);

			repuesto._getDetallePedidos().add(detalle);
			pedido._getDetallePedidos().add(detalle);
		}

		public static
				void unlink(
						DetallePedido detalle) {
			detalle.getPedido()._getDetallePedidos().remove(detalle);
			detalle.getRepuesto()._getDetallePedidos().remove(detalle);

			detalle._setRepuesto(null);
			detalle._setPedido(null);
		}
	}

}
