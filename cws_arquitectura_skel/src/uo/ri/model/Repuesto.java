package uo.ri.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "TREPUESTOS")
public class Repuesto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String codigo;
	private String descripcion;
	private double precio;

	private int existencias;

	@Column(name = "max_existencias")
	private int maxExistencias;
	@Column(name = "min_existencias")
	private int minExistencias;

	@OneToMany(mappedBy = "repuesto")
	private Set<Sustitucion> sustituciones = new HashSet<>();

	@OneToMany(mappedBy = "repuesto")
	private Set<Suministro> suministros = new HashSet<>();

	@OneToMany(mappedBy = "repuesto")
	private Set<DetallePedido> detallePedidos = new HashSet<>();

	Repuesto() {
	}

	public Repuesto(String codigo) {
		super();
		this.codigo = codigo;
	}

	public Repuesto(String codigo, String descripcion, double precio) {
		this(codigo);
		this.descripcion = descripcion;
		this.precio = precio;
	}

	public
			String getDescripcion() {
		return descripcion;
	}

	public
			void setDescripcion(
					String descripcion) {
		this.descripcion = descripcion;
	}

	public
			double getPrecio() {
		return precio;
	}

	public
			void setPrecio(
					double precio) {
		this.precio = precio;
	}

	public
			String getCodigo() {
		return codigo;
	}

	public
			Set<Sustitucion> getSustituciones() {
		return new HashSet<>(sustituciones);
	}

	Set<Sustitucion>
			_getSustituciones() {
		return sustituciones;
	}

	public
			int getExistencias() {
		return existencias;
	}

	public
			void setExistencias(
					int existencias) {
		this.existencias = existencias;
	}

	public
			int getMaxExistencias() {
		return maxExistencias;
	}

	public
			void setMaxExistencias(
					int maxExistencias) {
		this.maxExistencias = maxExistencias;
	}

	public
			int getMinExistencias() {
		return minExistencias;
	}

	public
			void setMinExistencias(
					int minExistencias) {
		this.minExistencias = minExistencias;
	}

	Set<DetallePedido>
			_getDetallePedidos() {
		return detallePedidos;
	}

	public
			Set<DetallePedido> getDetallePedidos() {
		return new HashSet<>(detallePedidos);
	}

	Set<Suministro>
			_getSuministros() {
		return suministros;
	}

	public
			Set<Suministro> getSuministros() {
		return new HashSet<>(suministros);
	}

	@Override
	public
			int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public
			boolean equals(
					Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Repuesto other = (Repuesto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public
			String toString() {
		return "Repuesto{" + "id=" + id + ", codigo='" + codigo + '\''
				+ ", descripcion='" + descripcion + '\'' + ", precio=" + precio
				+ ", existencias=" + existencias + ", max_existencias="
				+ maxExistencias + ", min_existencias=" + minExistencias
				+ ", sustituciones=" + sustituciones + '}';
	}

	public
			Proveedor providerMenorPrecio(
					List<Proveedor> proveedoresEnPedido) {
		if (getSuministros().size() == 0) {
			return null;
		}

		double minPrecio = getSuministros().stream()
				.mapToDouble(Suministro::getPrecio).min().getAsDouble();
		// Proveedores con el menor precio
		List<Proveedor> proveedores = getSuministros().stream()
				.filter(suministro -> suministro.getPrecio().equals(minPrecio))
				.map(Suministro::getProveedor).collect(Collectors.toList());
		// Proveedores con el menor precio y que estén en pedido
		List<Proveedor> proveedorOptimo =
				proveedores.stream().filter(proveedoresEnPedido::contains)
						.collect(Collectors.toList());
		if (proveedorOptimo.get(0) != null)
			return proveedorOptimo.get(0);
		else
			return proveedores.get(0);
	}

	public
			double getPrecioMin() {
		return getSuministros().stream().mapToDouble(Suministro::getPrecio)
				.min().getAsDouble();
	}

	public
			int unidadesAPedir() {
		return maxExistencias - existencias;
	}
}
