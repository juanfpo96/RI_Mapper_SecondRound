package uo.ri.model;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.types.CargoKey;
import uo.ri.model.types.FacturaStatus;

import javax.persistence.*;

@Entity
@IdClass(CargoKey.class)
@Table(name = "TCARGOS")
public class Cargo {

	@ManyToOne
	@Id
	private Factura factura;
	@ManyToOne
	@Id
	private MedioPago medioPago;
	private double importe = 0.0;

	Cargo() {
	}

	public Cargo(Factura factura, MedioPago medioPago, double importe)
			throws BusinessException {
		// incrementar el importe en el acumulado del medio de pago
		// guardar el importe
		// enlazar (link) factura, este cargo y medioDePago
		medioPago.incrementarAcumulado(importe);
		this.importe = importe;
		Association.Cargar.link(medioPago, this, factura);
	}

	public
			Factura getFactura() {
		return factura;
	}

	void
			_setFactura(
					Factura factura) {
		this.factura = factura;
	}

	public
			MedioPago getMedioPago() {
		return medioPago;
	}

	void
			_setMedioPago(
					MedioPago medioPago) {
		this.medioPago = medioPago;
	}

	public
			double getImporte() {
		return importe;
	}

	/**
	 * Anula (retrocede) este cargo de la factura y el medio de pago Solo se
	 * puede hacer si la factura no esta abonada Decrementar el acumulado del
	 * medio de pago Desenlazar el cargo de la factura y el medio de pago
	 *
	 * @throws BusinessException
	 */
	public
			void rewind()
					throws BusinessException {
		// verificar que la factura no esta ABONADA
		// decrementar acumulado en medio de pago
		// desenlazar factura, cargo y medio de pago
		if (factura.getStatus() != FacturaStatus.ABONADA) {
			medioPago.decrementarAcumulado(importe);
			Association.Cargar.unlink(this);
		}

	}

}
