package uo.ri.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Table(name = "TMEDIOSPAGO")
@DiscriminatorColumn(name = "DTYPE")
// no tiene sentido cuando hay JOINED
public abstract class MedioPago {

	protected double acumulado = 0.0;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne
	private Cliente cliente;
	@OneToMany(mappedBy = "medioPago")
	private Set<Cargo> cargos = new HashSet<>();

	public
			double getAcumulado() {
		return acumulado;
	}

	public
			Cliente getCliente() {
		return cliente;
	}

	void
			_setCliente(
					Cliente cliente) {
		this.cliente = cliente;
	}

	public
			Set<Cargo> getCargos() {
		return new HashSet<>(cargos);
	}

	Set<Cargo>
			_getCargos() {
		return cargos;
	}

	public
			void incrementarAcumulado(
					double importe) {
		acumulado += importe;
	}

	public
			void decrementarAcumulado(
					double importe) {
		acumulado -= importe;
	}

	@Override
	public
			int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		return result;
	}

	@Override
	public
			boolean equals(
					Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MedioPago other = (MedioPago) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		return true;
	}

}
