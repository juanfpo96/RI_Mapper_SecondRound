package uo.ri.model;

import uo.ri.model.types.DetallePedidoKey;

import javax.persistence.*;

/**
 * Created by Fran on 03/01/2017.
 */
@Entity
@Table(name = "TDetallePedido")
@IdClass(DetallePedidoKey.class)
public class DetallePedido {
	@Id
	@ManyToOne
	@JoinColumn(name = "pedido_id")
	private Pedido pedido;

	@Id
	@ManyToOne
	@JoinColumn(name = "repuesto_id")
	private Repuesto repuesto;

	private int unidades;
	private double precio;

	DetallePedido() {
	}

	public DetallePedido(Repuesto repuesto, Pedido pedido) {
		Association.Detallar.link(this, pedido, repuesto);
		this.precio = repuesto.getPrecioMin();
		this.unidades = repuesto.unidadesAPedir();
		this.pedido._getDetallePedidos().add(this);

	}

	public
			int getUnidades() {
		return unidades;
	}

	public
			void setUnidades(
					int unidades) {
		this.unidades = unidades;
	}

	public
			Double getPrecio() {
		return precio;
	}

	public
			void setPrecio(
					double precio) {
		this.precio = precio;
	}

	public
			Repuesto getRepuesto() {
		return repuesto;
	}

	public
			Pedido getPedido() {
		return pedido;
	}

	@Override
	public
			boolean equals(
					Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		DetallePedido that = (DetallePedido) o;

		if (!pedido.equals(that.pedido))
			return false;
		return repuesto.equals(that.repuesto);
	}

	@Override
	public
			int hashCode() {
		int result = pedido.hashCode();
		result = 31 * result + repuesto.hashCode();
		return result;
	}

	@Override
	public
			String toString() {
		return "DetallePedido{" + "pedido=" + pedido + ", repuesto=" + repuesto
				+ ", unidades=" + unidades + ", precio=" + precio + '}';
	}
	
	

	public
			void _setPedido(
					Pedido pedido) {
		this.pedido = pedido;
	}

	public
			void _setRepuesto(
					Repuesto repuesto) {
		this.repuesto = repuesto;
	}

	public
			double mediaPonderada() {
		return ((this.getPrecio() * this.getUnidades())
				+ (repuesto.getPrecio() * repuesto.getExistencias()))
				/ (getUnidades() + repuesto.getExistencias());
	}

	public
			int getTotalUnidades() {
		return getUnidades() + repuesto.getExistencias();
	}
}
