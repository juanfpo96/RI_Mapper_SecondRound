package uo.ri.model;

import uo.ri.model.types.PedidoStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Fran on 03/01/2017.
 */
@Entity
@Table(name = "TPedidos")
public class Pedido {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String codigo;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_creacion")
	private Date fechaCreacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_recepcion")
	private Date fechaRecepcion;

	@Enumerated(EnumType.STRING)
	private PedidoStatus estado;

	@ManyToOne
	@JoinColumn(name = "proveedor_id")
	private Proveedor proveedor;

	@OneToMany(mappedBy = "pedido")
	private Set<DetallePedido> detallePedidos = new HashSet<>();

	Pedido() {
	}

	public Pedido(Proveedor proveedor, String codigo) {
		this(proveedor);
		this.codigo = codigo;
	}

	public Pedido(Proveedor p) {
		Association.Proveer.link(p, this);
		this.fechaCreacion = new Date();
		this.proveedor = p;
		p._getPedidos().add(this);
		this.codigo = p.getCodigoPedidoNuevo();
		this.estado = PedidoStatus.REALIZADO;
	}

	@Override
	public
			boolean equals(
					Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pedido other = (Pedido) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public
			int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	public
			Long getId() {
		return id;
	}

	public
			Proveedor getProveedor() {
		return proveedor;
	}

	public
			String getCodigo() {
		return codigo;
	}

	public
			PedidoStatus getEstado() {
		return estado;
	}

	public
			void setEstado(
					PedidoStatus estado) {
		this.estado = estado;
	}

	public
			Date getFechaCreacion() {
		return fechaCreacion;
	}

	public
			Date getFechaRecepcion() {
		return fechaRecepcion;
	}

	public
			void setFechaRecepcion(
					Date fechaRecepcion) {
		this.fechaRecepcion = fechaRecepcion;
	}

	Set<DetallePedido>
			_getDetallePedidos() {
		return detallePedidos;
	}

	public
			Set<DetallePedido> getDetallePedidos() {
		return new HashSet<>(detallePedidos);
	}

	void
			_setProveedor(
					Proveedor proveedor) {
		this.proveedor = proveedor;
	}

}
