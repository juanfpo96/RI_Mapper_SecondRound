package uo.ri.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Fran on 03/01/2017.
 */
@Entity
@Table(name = "TProviders")
public class Proveedor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	private String nombre;
	private String codigo;

	@OneToMany(mappedBy = "proveedor")
	private Set<Suministro> suministros = new HashSet<>();

	@OneToMany(mappedBy = "proveedor")
	private Set<Pedido> pedidos = new HashSet<>();

	public Proveedor(String codigo, String nombre) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
	}

	Proveedor() {
	}

	public
			Long getId() {
		return id;
	}

	public
			String getNombre() {
		return nombre;
	}

	public
			void setNombre(
					String nombre) {
		this.nombre = nombre;
	}

	public
			String getCodigo() {
		return codigo;
	}

	public
			Set<Pedido> getPedidos() {
		return new HashSet<>(pedidos);
	}

	Set<Pedido>
			_getPedidos() {
		return pedidos;
	}

	public
			Set<Suministro> getSuministros() {
		return new HashSet<>(suministros);
	}

	Set<Suministro>
			_getSuministros() {
		return suministros;
	}

	Set<Suministro>
			_getSuministradores() {
		return suministros;
	}

	public
			String getCodigoPedidoNuevo() {
		return codigo + pedidos.size();
	}

	@Override
	public
			boolean equals(
					Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proveedor other = (Proveedor) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}

	@Override
	public
			int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public
			String toString() {
		return "Proveedor{" + "nombre='" + nombre + '\'' + ", codigo='" + codigo
				+ '\'' + '}';
	}
}
