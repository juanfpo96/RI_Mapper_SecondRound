package uo.ri.model;

import uo.ri.model.types.AveriaStatus;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TAVERIAS")
public class Averia {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String descripcion;
	@Temporal(TemporalType.DATE)
	private Date fecha;
	private double importe = 0.0;
	@Enumerated(EnumType.STRING)
	private AveriaStatus status = AveriaStatus.ABIERTA;

	@ManyToOne
	private Vehiculo vehiculo;
	@ManyToOne
	private Mecanico mecanico;
	@ManyToOne
	private Factura factura;

	@OneToMany(mappedBy = "averia")
	private Set<Intervencion> intervenciones = new HashSet<>();

	Averia() {
	}

	public Averia(Date fecha, Vehiculo vehiculo) {
		super();
		this.fecha = fecha;
		Association.Averiar.link(vehiculo, this);
		vehiculo.incrementarNumAverias();
	}

	public Averia(Vehiculo vehiculo, String descripcion) {
		this(new Date(), vehiculo);
		this.descripcion = descripcion;
	}

	public
			String getDescripcion() {
		return descripcion;
	}

	public
			void setDescripcion(
					String descripcion) {
		this.descripcion = descripcion;
	}

	public
			double getImporte() {
		return importe;
	}

	public
			AveriaStatus getStatus() {
		return status;
	}

	void
			_setStatus(
					AveriaStatus status) {
		this.status = status;
	}

	public
			Date getFecha() {
		return fecha;
	}

	public
			Vehiculo getVehiculo() {
		return vehiculo;
	}

	/* package */ void
			_setVehiculo(
					Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public
			Set<Intervencion> getIntervenciones() {
		return new HashSet<>(intervenciones);
	}

	Set<Intervencion>
			_getIntervenciones() {
		return intervenciones;
	}

	public
			Mecanico getMecanico() {
		return mecanico;
	}

	void
			_setMecanico(
					Mecanico mecanico) {
		this.mecanico = mecanico;
	}

	public
			Factura getFactura() {
		return factura;
	}

	void
			_setFactura(
					Factura factura) {
		this.factura = factura;
	}

	@Override
	public
			int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result
				+ ((vehiculo == null) ? 0 : vehiculo.hashCode());
		return result;
	}

	@Override
	public
			boolean equals(
					Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Averia other = (Averia) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (vehiculo == null) {
			if (other.vehiculo != null)
				return false;
		} else if (!vehiculo.equals(other.vehiculo))
			return false;
		return true;
	}

	@Override
	public
			String toString() {
		return "Averia [descripcion=" + descripcion + ", fecha=" + fecha
				+ ", importe=" + importe + ", status=" + status + ", vehiculo="
				+ vehiculo + "]";
	}

	/**
	 * Asigna la averia al mecanico
	 *
	 * @param mecanico
	 */
	public
			void assignTo(
					Mecanico mecanico) {
		// Solo se puede asignar una averia que está ABIERTA
		// linkado de averia y mecanico
		// la averia pasa a ASIGNADA
		if (getStatus() == AveriaStatus.ABIERTA) {
			Association.Asignar.link(mecanico, this);
			this.status = AveriaStatus.ASIGNADA;
		}
	}

	/**
	 * El mecanico da por finalizada esta averia, entonces se calcula el importe
	 */
	public
			void markAsFinished() {
		// Se verifica que está en estado ASIGNADA
		// se calcula el importe
		// se desvincula mecanico y averia
		// el status cambia a TERMINADA
		if (getStatus() == AveriaStatus.ASIGNADA) {
			calcularImporte();
			Association.Asignar.unlink(mecanico, this);
			_setStatus(AveriaStatus.TERMINADA);
		}
	}

	private
			void calcularImporte() {
		importe = 0;
		for (Intervencion intervencion : intervenciones) {
			importe += intervencion.getImporte();
		}
	}

	/**
	 * Una averia en estado TERMINADA se puede asignar a otro mecanico (el
	 * primero no ha podido terminar la reparación), pero debe ser pasada a
	 * ABIERTA primero
	 */
	public
			void reopen() {
		// Solo se puede reabrir una averia que esta TERMINADA
		// la averia pasa a ABIERTA
		if (getStatus() == AveriaStatus.TERMINADA)
			_setStatus(AveriaStatus.ABIERTA);
	}

	/**
	 * Una averia ya facturada se elimina de la factura
	 */
	public
			void markBackToFinished() {
		// verificar que la averia esta FACTURADA
		// cambiar status a TERMINADA
		if (getStatus() == AveriaStatus.FACTURADA)
			_setStatus(AveriaStatus.TERMINADA);
	}

	public
			void markAsInvoiced() {
		_setStatus(AveriaStatus.FACTURADA);
	}

	public
			void desassign() {
		if (getStatus() == AveriaStatus.ASIGNADA) {
			Association.Asignar.unlink(mecanico, this);
			_setStatus(AveriaStatus.ABIERTA);
		}
	}

	public
			long getId() {
		return id;
	}
}
