package uo.ri.business;

import uo.ri.model.*;
import uo.ri.model.exception.BusinessException;

import java.util.List;

public interface AdminService {

	void newMechanic(
			Mecanico mecanico)
			throws BusinessException;

	void deleteMechanic(
			Long idMecanico)
			throws BusinessException;

	void updateMechanic(
			Mecanico mecanico)
			throws BusinessException;

	Mecanico findMechanicById(
			Long id)
			throws BusinessException;

	List<Mecanico> findAllMechanics()
			throws BusinessException;

	void newProvider(
			Proveedor proveedor)
			throws BusinessException;

	List<Proveedor> findAllProviders()
			throws BusinessException;

	void deleteProvider(
			Long idProveedor)
			throws BusinessException;

	Proveedor findProviderById(
			Long id)
			throws BusinessException;

	void updateProvider(
			Proveedor p)
			throws BusinessException;

	Repuesto findRepuestoById(
			Long idRepuesto)
			throws BusinessException;

	void newSuministro(
			Suministro suministro)
			throws BusinessException;

	void deleteSuministro(
			Long idRepuesto, Long idProveedor)
			throws BusinessException;

	Suministro findSuministroById(
			Long idRepuesto, Long idProveedor)
			throws BusinessException;

	void updateSuministro(
			Suministro s)
			throws BusinessException;

	Pedido findPedidoByCodigo(
			String codigo)
			throws BusinessException;

	void UpdatePedido(
			Pedido pedido)
			throws BusinessException;

	Proveedor findProviderByCodigo(
			String codigo)
			throws BusinessException;

	void deleteRepuesto(
			Long idProveedor)
			throws BusinessException;

	void updateRepuesto(
			Repuesto p)
			throws BusinessException;

	void newRepuesto(
			Repuesto r)
			throws BusinessException;

	List<Pedido> generarPedido()
			throws BusinessException;

	Repuesto findRepuestoByCodigo(String codigo)
			throws BusinessException;

	List<Repuesto> findRepuestoByDescripcion(String descripcion)
			throws BusinessException;

	List<Repuesto> findRepuestoExistenciasBajas()
			throws BusinessException;

	// resto de métodos que faltan...

}
