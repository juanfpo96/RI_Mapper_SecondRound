package uo.ri.business.impl.cash;

import uo.ri.model.Averia;
import uo.ri.model.Factura;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.AveriaFinder;
import uo.ri.persistence.FacturaFinder;

import java.util.List;

public class CreateInvoiceFor implements Command {

	private List<Long> idsAveria;

	public CreateInvoiceFor(List<Long> idsAveria) {
		this.idsAveria = idsAveria;
	}

	public Factura execute()
			throws BusinessException {
		long numero = FacturaFinder.getNextInvoiceNumber();
		List<Averia> averias = AveriaFinder.findByIds(idsAveria);
		Factura factura = new Factura(numero, averias);
		return factura;
	}

}
