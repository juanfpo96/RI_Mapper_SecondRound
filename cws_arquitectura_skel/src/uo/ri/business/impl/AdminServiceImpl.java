package uo.ri.business.impl;

import uo.ri.business.AdminService;
import uo.ri.business.CommandExecutor;
import uo.ri.business.impl.admin.*;
import uo.ri.model.*;
import uo.ri.model.exception.BusinessException;
import uo.ri.persistence.util.Jpa;

import java.util.List;

public class AdminServiceImpl implements AdminService {

	private CommandExecutor executor = new CommandExecutor();

	@Override public void newMechanic(
			Mecanico mecanico)
			throws BusinessException {
		executor.execute(new AddMechanic(mecanico));
	}

	@Override public void newSuministro(
			Suministro suministro)
			throws BusinessException {
		executor.execute(new AddSuministro(suministro));
	}

	@Override public void newProvider(
			Proveedor proveedor)
			throws BusinessException {
		executor.execute(new AddProvider(proveedor));
	}

	@Override public void newRepuesto(
			Repuesto r)
			throws BusinessException {
		executor.execute(new AddRepuesto(r));
	}

	@Override public void updateMechanic(
			Mecanico mecanico)
			throws BusinessException {
		executor.execute(() -> Jpa.getManager().merge(mecanico));
	}

	@Override public void updateProvider(
			Proveedor p)
			throws BusinessException {
		executor.execute(() -> Jpa.getManager().merge(p));
	}

	@Override public void deleteMechanic(
			Long idMecanico)
			throws BusinessException {
		executor.execute(new DeleteMechanic(idMecanico));
	}

	@Override public void deleteProvider(
			Long idProveedor)
			throws BusinessException {
		executor.execute(new DeleteProvider(idProveedor));
	}

	@Override public void deleteSuministro(
			Long idRepuesto, Long idProveedor)
			throws BusinessException {
		executor.execute(new DeleteSuministro(idRepuesto, idProveedor));
	}

	@SuppressWarnings("unchecked")
	@Override public List<Mecanico> findAllMechanics()
			throws BusinessException {
		return (List<Mecanico>) executor.execute(new FindAllMechanics());
	}

	@SuppressWarnings("unchecked")
	@Override public List<Proveedor> findAllProviders()
			throws BusinessException {
		return (List<Proveedor>) executor.execute(new FindAllProviders());
	}

	@SuppressWarnings("unchecked")
	@Override public List<Pedido> generarPedido()
			throws BusinessException {
		return (List<Pedido>) executor.execute(new GenerarPedido());
	}

	@Override public Repuesto findRepuestoByCodigo(String codigo)
			throws BusinessException {
		return (Repuesto) executor.execute(new FindRepuestoByCodigo(codigo));
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Repuesto> findRepuestoByDescripcion(String descripcion)
			throws BusinessException {
		return (List<Repuesto>) executor
				.execute(new FindRepuestoByDescripcion(descripcion));
	}

	@SuppressWarnings("unchecked")
	@Override public List<Repuesto> findRepuestoExistenciasBajas()
			throws BusinessException {
		return (List<Repuesto>) executor
				.execute(new FindRepuestoExistenciasBajas());
	}

	@Override public Proveedor findProviderById(
			Long id)
			throws BusinessException {
		return (Proveedor) executor.execute(new FindProveedorById(id));
	}

	@Override public Repuesto findRepuestoById(
			Long id)
			throws BusinessException {
		return (Repuesto) executor.execute(new FindRepuestoById(id));
	}

	@Override public Mecanico findMechanicById(
			Long id)
			throws BusinessException {
		return (Mecanico) executor.execute(new FindMechanicById(id));
	}

	@Override public Suministro findSuministroById(
			Long idRepuesto, Long idProveedor)
			throws BusinessException {
		return (Suministro) executor
				.execute(new FindSuministroById(idRepuesto, idProveedor));
	}

	@Override public void updateSuministro(
			Suministro s)
			throws BusinessException {
		executor.execute(() -> Jpa.getManager().merge(s));
	}

	@Override public Pedido findPedidoByCodigo(
			String codigo)
			throws BusinessException {
		return (Pedido) executor.execute(new FindPedidoByCodigo(codigo));
	}

	@Override public void UpdatePedido(
			Pedido pedido)
			throws BusinessException {
		executor.execute(new UpdatePedido(pedido));
	}

	@Override public Proveedor findProviderByCodigo(
			String codigo)
			throws BusinessException {
		return (Proveedor) executor.execute(new FindProviderByCodigo(codigo));
	}

	@Override public void deleteRepuesto(
			Long idRepuesto)
			throws BusinessException {
		executor.execute(new DeleteRepuesto(idRepuesto));
	}

	@Override public void updateRepuesto(
			Repuesto p)
			throws BusinessException {
		executor.execute(() -> Jpa.getManager().merge(p));
	}

}
