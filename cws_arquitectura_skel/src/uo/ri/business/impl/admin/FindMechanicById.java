package uo.ri.business.impl.admin;

import uo.ri.model.Mecanico;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.MecanicoFinder;

public class FindMechanicById implements Command {

	private Long id;

	public FindMechanicById(Long id) {
		this.id = id;
	}

	public Object execute()
			throws BusinessException {

		Mecanico mecanico = MecanicoFinder.findById(id);
		return mecanico;
	}

}
