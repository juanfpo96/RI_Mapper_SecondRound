package uo.ri.business.impl.admin;

import uo.ri.model.Pedido;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.model.types.PedidoStatus;
import uo.ri.persistence.util.Jpa;

/**
 * Created by Fran on 05/01/2017.
 */
public class UpdateStatusRecibidoPedido implements Command {
	private Pedido pedido;

	public UpdateStatusRecibidoPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override public Object execute()
			throws BusinessException {
		pedido.setEstado(PedidoStatus.SERVIDO);
		Jpa.getManager().merge(pedido);
		return pedido;
	}
}
