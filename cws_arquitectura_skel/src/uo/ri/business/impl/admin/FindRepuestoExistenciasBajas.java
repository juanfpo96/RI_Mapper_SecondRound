package uo.ri.business.impl.admin;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.RepuestoFinder;

/**
 * Created by juanf on 12/05/2017.
 */
public class FindRepuestoExistenciasBajas implements Command {

	@Override public Object execute()
			throws BusinessException {
		Object repuesto = RepuestoFinder.findExistenciasBajas();
		if (repuesto != null)
			return repuesto;
		else
			throw new BusinessException(
					"No existen repuestos con existencias bajas en BBDD");
	}
}
