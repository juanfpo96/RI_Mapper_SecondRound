package uo.ri.business.impl.admin;

import uo.ri.model.Mecanico;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

public class DeleteMechanic implements Command {

	private Long idMecanico;

	public DeleteMechanic(Long idMecanico) {
		this.idMecanico = idMecanico;
	}

	public Object execute()
			throws BusinessException {
		Mecanico m = Jpa.getManager().find(Mecanico.class, idMecanico);
		assertNotNull(m);
		assertSinIntervenciones(m);
		assertSinAsignadas(m);
		Jpa.getManager().remove(m);
		return m;
	}

	private void assertNotNull(
			Mecanico m)
			throws BusinessException {
		if (m != null)
			return;
		throw new BusinessException("El mecanico no existe");
	}

	private void assertSinAsignadas(
			Mecanico m)
			throws BusinessException {
		if (m.getAsignadas().size() > 0) {
			throw new BusinessException(
					"El mecanico tiene averias asignadas. No se puede eliminar"
							+ ".");
		}
	}

	private void assertSinIntervenciones(
			Mecanico m)
			throws BusinessException {
		if (m.getIntervenciones().size() > 0) {
			throw new BusinessException(
					"El mecanico tiene intervenciones disponibles. No se puede"
							+ " eliminar.");
		}
	}

}
