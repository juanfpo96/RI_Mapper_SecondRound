package uo.ri.business.impl.admin;

import uo.ri.model.Proveedor;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

/**
 * Created by Fran on 04/01/2017.
 */
public class DeleteProvider implements Command {
	private Long idProveedor;

	public DeleteProvider(Long idProveedor) {
		this.idProveedor = idProveedor;
	}

	public Object execute()
			throws BusinessException {
		Proveedor p = Jpa.getManager().find(Proveedor.class, idProveedor);
		assertNotNull(p);
		assertSinPedidos(p);
		assertSinSuministros(p);
		Jpa.getManager().remove(p);
		return p;
	}

	private void assertNotNull(
			Proveedor p)
			throws BusinessException {
		if (p != null)
			return;
		throw new BusinessException("El proveedor no existe");
	}

	private void assertSinSuministros(
			Proveedor p)
			throws BusinessException {
		if (p.getSuministros().size() > 0) {
			throw new BusinessException(
					"El proveedor tiene suministros asignados. No se puede "
							+ "eliminar.");
		}
	}

	private void assertSinPedidos(
			Proveedor p)
			throws BusinessException {
		if (p.getPedidos().size() > 0) {
			throw new BusinessException(
					"El proveedor tiene pedidos asignados. No se puede "
							+ "eliminar.");
		}
	}
}
