package uo.ri.business.impl.admin;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.PedidoFinder;

/**
 * Created by Fran on 05/01/2017.
 */
public class FindPedidoByCodigo implements Command {

	private String codigo;

	public FindPedidoByCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override public Object execute()
			throws BusinessException {
		Object pedido = PedidoFinder.findByCodigo(codigo);
		if (pedido != null)
			return pedido;
		else
			throw new BusinessException("El pedido no existe en la BBDD");
	}
}
