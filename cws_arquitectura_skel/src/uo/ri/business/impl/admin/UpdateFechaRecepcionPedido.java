package uo.ri.business.impl.admin;

import uo.ri.model.Pedido;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;

/**
 * Created by Fran on 05/01/2017.
 */
public class UpdateFechaRecepcionPedido implements Command {
	private Pedido pedido;

	public UpdateFechaRecepcionPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override public Object execute()
			throws BusinessException {

		return pedido;
	}
}
