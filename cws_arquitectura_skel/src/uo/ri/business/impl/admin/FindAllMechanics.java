package uo.ri.business.impl.admin;

import uo.ri.model.Mecanico;
import uo.ri.model.exception.Command;
import uo.ri.persistence.MecanicoFinder;

import java.util.List;

public class FindAllMechanics implements Command {

	public List<Mecanico> execute() {

		return MecanicoFinder.findAll();

	}

}
