package uo.ri.business.impl.admin;

import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.RepuestoFinder;

/**
 * Created by Fran on 04/01/2017.
 */
public class FindRepuestoById implements Command {

	private Long id;

	public FindRepuestoById(Long id) {
		this.id = id;
	}

	@Override public Object execute()
			throws BusinessException {
		Repuesto repuesto = RepuestoFinder.findById(id);
		return repuesto;
	}
}
