package uo.ri.business.impl.admin;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.ProviderFinder;

/**
 * Created by Fran on 04/01/2017.
 */
public class FindAllProviders implements Command {
	@Override public Object execute()
			throws BusinessException {
		return ProviderFinder.findAll();
	}
}
