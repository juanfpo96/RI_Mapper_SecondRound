package uo.ri.business.impl.admin;

import uo.ri.model.Suministro;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.model.types.SuministroKey;
import uo.ri.persistence.util.Jpa;

/**
 * Created by Fran on 04/01/2017.
 */
public class DeleteSuministro implements Command {
	private Long idRepuesto;
	private Long idProveedor;

	public DeleteSuministro(Long idRepuesto, Long idProveedor) {
		this.idRepuesto = idRepuesto;
		this.idProveedor = idProveedor;
	}

	public Object execute()
			throws BusinessException {
		Suministro s = Jpa.getManager().find(Suministro.class,
				new SuministroKey(idProveedor, idRepuesto));
		assertNotNull(s);
		Jpa.getManager().remove(s);
		return s;
	}

	private void assertNotNull(
			Suministro s)
			throws BusinessException {
		if (s != null)
			return;
		throw new BusinessException("El mecanico no existe");
	}
}
