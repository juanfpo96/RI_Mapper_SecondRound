package uo.ri.business.impl.admin;

import uo.ri.model.DetallePedido;
import uo.ri.model.Pedido;
import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.model.types.PedidoStatus;
import uo.ri.persistence.util.Jpa;

import java.util.Date;
import java.util.Set;

/**
 * Created by Fran on 05/01/2017.
 */
public class UpdatePreciosUnidadesPedido implements Command {

	private Pedido pedido;

	public UpdatePreciosUnidadesPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	@Override public Object execute()
			throws BusinessException {
		// Update precio unidades pedido
		Set<DetallePedido> detalle = pedido.getDetallePedidos();

		for (DetallePedido dP : detalle) {
			Repuesto r = dP.getRepuesto();
			r.setPrecio(dP.mediaPonderada());
			r.setExistencias(dP.getTotalUnidades());
			Jpa.getManager().merge(r);
		}
		// Update fecha y status pedido
		pedido.setFechaRecepcion(new Date());
		pedido.setEstado(PedidoStatus.SERVIDO);
		Jpa.getManager().merge(pedido);

		return pedido;
	}
}
