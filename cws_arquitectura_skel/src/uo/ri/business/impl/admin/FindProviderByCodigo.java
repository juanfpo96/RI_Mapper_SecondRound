package uo.ri.business.impl.admin;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.ProviderFinder;

/**
 * Created by Fran on 05/01/2017.
 */
public class FindProviderByCodigo implements Command {
	private String codigo;

	public FindProviderByCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override public Object execute()
			throws BusinessException {
		Object provider = ProviderFinder.findByCodigo(codigo);
		if (provider != null)
			return provider;
		else
			throw new BusinessException("El proveedor no existe en la BBDD");
	}
}
