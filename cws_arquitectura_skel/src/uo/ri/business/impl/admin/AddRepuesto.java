package uo.ri.business.impl.admin;

import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

/**
 * Created by juanf on 23/04/2017.
 */
public class AddRepuesto implements Command {
	private final Repuesto repuesto;

	public AddRepuesto(Repuesto repuesto) {
		this.repuesto = repuesto;
	}

	@Override public Object execute()
			throws BusinessException {
		Jpa.getManager().persist(repuesto);
		return repuesto;
	}
}
