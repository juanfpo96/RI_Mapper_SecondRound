package uo.ri.business.impl.admin;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.RepuestoFinder;

/**
 * Created by juanf on 12/05/2017.
 */
public class FindRepuestoByDescripcion implements Command {
	private String descripcion;

	public FindRepuestoByDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override public Object execute()
			throws BusinessException {
		Object repuesto = RepuestoFinder.findByDescripcion(descripcion);
		if (repuesto != null)
			return repuesto;
		else
			throw new BusinessException(
					"No existen repuestos con esa descripción en BBDD");
	}
}
