package uo.ri.business.impl.admin;

import uo.ri.model.Suministro;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

/**
 * Created by Fran on 04/01/2017.
 */
public class AddSuministro implements Command {
	private Suministro suministro;

	public AddSuministro(Suministro suministro) {
		this.suministro = suministro;
	}

	public Object execute() {
		Jpa.getManager().persist(suministro);
		return suministro;
	}
}
