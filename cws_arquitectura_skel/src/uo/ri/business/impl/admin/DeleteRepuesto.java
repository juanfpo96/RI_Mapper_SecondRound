package uo.ri.business.impl.admin;

import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

/**
 * Created by juanf on 22/04/2017.
 */
public class DeleteRepuesto implements Command {
	private Long idRepuesto;

	public DeleteRepuesto(Long idRepuesto) {
		this.idRepuesto = idRepuesto;
	}

	public Object execute()
			throws BusinessException {
		Repuesto p = Jpa.getManager().find(Repuesto.class, idRepuesto);
		assertNotNull(p);
		assertSinDetallePedido(p);
		assertSinSuministros(p);
		assertSinSustituciones(p);
		Jpa.getManager().remove(p);
		return p;
	}

	private void assertSinSustituciones(
			Repuesto p)
			throws BusinessException {
		if (p.getSustituciones().size() > 0)
			throw new BusinessException("El repuesto tiene sustituciones");
	}

	private void assertSinSuministros(
			Repuesto p)
			throws BusinessException {
		if (p.getSuministros().size() > 0)
			throw new BusinessException("El repuesto tiene suministros");
	}

	private void assertSinDetallePedido(
			Repuesto p)
			throws BusinessException {
		if (p.getDetallePedidos().size() > 0)
			throw new BusinessException("El repuesto tiene detalles pedido");
	}

	private void assertNotNull(
			Repuesto s)
			throws BusinessException {
		if (s != null)
			return;
		throw new BusinessException("El repuesto no existe");
	}
}
