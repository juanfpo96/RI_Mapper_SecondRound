package uo.ri.business.impl.admin;

import uo.ri.model.Mecanico;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

@Deprecated
/**
 * Esta clase queda deprecada por el uso preferente de lambda functions (JDK
 * 1.8).
 */ public class UpdateMechanic implements Command {

	private Mecanico mecanico;

	public UpdateMechanic(Mecanico mecanico) {
		this.mecanico = mecanico;
	}

	public Object execute()
			throws BusinessException {
		Jpa.getManager().merge(mecanico);
		return mecanico;
	}

}
