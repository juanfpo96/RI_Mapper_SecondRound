package uo.ri.business.impl.admin;

import uo.ri.model.Proveedor;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.ProviderFinder;

/**
 * Created by Fran on 04/01/2017.
 */

public class FindProveedorById implements Command {
	private Long id;

	public FindProveedorById(Long id) {
		this.id = id;
	}

	@Override public Object execute()
			throws BusinessException {
		Proveedor proveedor = ProviderFinder.findById(id);
		return proveedor;
	}
}
