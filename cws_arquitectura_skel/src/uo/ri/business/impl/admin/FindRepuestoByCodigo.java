package uo.ri.business.impl.admin;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.RepuestoFinder;

/**
 * Created by juanf on 12/05/2017.
 */
public class FindRepuestoByCodigo implements Command {
	private String codigo;

	public FindRepuestoByCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override public Object execute()
			throws BusinessException {
		Object repuesto = RepuestoFinder.findByCodigo(codigo);
		if (repuesto != null)
			return repuesto;
		else
			throw new BusinessException("El repuesto no existe en la BBDD");
	}
}
