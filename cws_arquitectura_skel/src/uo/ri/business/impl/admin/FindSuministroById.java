package uo.ri.business.impl.admin;

import uo.ri.model.Suministro;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.SuministroFinder;

/**
 * Created by Fran on 04/01/2017.
 */
public class FindSuministroById implements Command {
	private Long idProveedor;
	private Long idRepuesto;

	public FindSuministroById(Long idRepuesto, Long idProveedor) {
		this.idProveedor = idProveedor;
		this.idRepuesto = idRepuesto;
	}

	@Override public Object execute()
			throws BusinessException {
		Suministro suministro =
				SuministroFinder.findByID(idProveedor, idRepuesto);
		return suministro;
	}
}
