package uo.ri.business.impl.admin;

import uo.ri.model.DetallePedido;
import uo.ri.model.Pedido;
import uo.ri.model.Proveedor;
import uo.ri.model.Repuesto;
import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.RepuestoFinder;
import uo.ri.persistence.util.Jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by juanf on 23/04/2017.
 */
public class GenerarPedido implements Command {

	List<Pedido> pedidos;

	public GenerarPedido() {
		this.pedidos = new ArrayList<>();
	}

	@Override public Object execute()
			throws BusinessException {

		List<Repuesto> repuestosExistenciasBajas =
				RepuestoFinder.findExistenciasBajas();

		if (repuestosExistenciasBajas.isEmpty()) {
			throw new BusinessException(
					"No hay repuestos con existencias bajas");
		}

		Proveedor[] pro = new Proveedor[1];
		for (Repuesto repuesto : repuestosExistenciasBajas) {
			pro[0] = repuesto.providerMenorPrecio(
					pedidos.stream().map(Pedido::getProveedor)
							.collect(Collectors.toList()));

			if (pro[0] == null) {
				throw new BusinessException(
						"El repuesto " + repuesto.getCodigo()
								+ " no tiene proveedores");
			}
			Optional<Pedido> pedido = pedidos.stream()
					.filter(pedido1 -> pedido1.getProveedor().equals(pro[0]))
					.findAny();
			Pedido pedidoActual;
			if (!pedido.isPresent()) {
				pedidoActual = new Pedido(pro[0]);
				pedidos.add(pedidoActual);
				Jpa.getManager().persist(pedidoActual);
			} else {
				pedidoActual = pedido.get();
			}
			Jpa.getManager().persist(new DetallePedido(repuesto,
					pedidoActual));
		}

		return pedidos;
	}
}
