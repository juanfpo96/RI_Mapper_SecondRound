package uo.ri.business.impl.admin;

import uo.ri.model.Proveedor;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

/**
 * Created by Fran on 04/01/2017.
 */
public class AddProvider implements Command {

	private Proveedor proveedor;

	public AddProvider(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Object execute() {
		Jpa.getManager().persist(proveedor);
		return proveedor;
	}
}
