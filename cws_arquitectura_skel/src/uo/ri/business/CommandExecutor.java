package uo.ri.business;

import uo.ri.model.exception.BusinessException;
import uo.ri.model.exception.Command;
import uo.ri.persistence.util.Jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceException;

public class CommandExecutor {

	public Object execute(
			Command cmd)
			throws BusinessException {
		Persistence.createEntityManagerFactory("carworkshop");
		EntityManager mapper = Jpa.createEntityManager();
		EntityTransaction trx = mapper.getTransaction();
		trx.begin();
		try {

			Object mObject = cmd.execute();

			trx.commit();
			return mObject;
		} catch (PersistenceException | BusinessException e) {
			if (trx.isActive())
				trx.rollback();
			throw e;
		} finally {
			if (mapper.isOpen())
				mapper.close();
		}
	}
}
